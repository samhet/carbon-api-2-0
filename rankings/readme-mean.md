How to generate the mean value for tests

1. Generate a new SQL table with the most recent tests for each unique url, for
the time period you are interested in.

This can be achieved with the following SQL statements

---

DROP TABLE IF EXISTS results;

CREATE TABLE `results` (
  `co2` decimal(18,16) DEFAULT NULL
);

INSERT into results
SELECT co2
FROM record
WHERE id IN (
    SELECT MAX(id)
    FROM record
    GROUP BY url
) AND timestamp > '2020-01-01 00:00:00'
ORDER BY co2 ASC;

SELECT AVG(co2) from results;
