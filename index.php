<?php

// -----------------------------------------------------------------------------
// Vendor code
// -----------------------------------------------------------------------------
require 'vendor/autoload.php';

// -----------------------------------------------------------------------------
// Carbon calculator code
// -----------------------------------------------------------------------------
if(file_exists('config.php')){
    require 'config.php';
}

require 'includes/traits/url.php';
require 'includes/traits/helpers.php';
require 'includes/database.php';
require 'includes/carbonapi.php';

new CarbonAPI;
